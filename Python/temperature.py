class TemperatureConverter():
    """
    A Function To Demonstrate "fruitful functions",

    read Allen B. Downey's book Think Python,
    Ch. 3 Functions: Fruitful Functions and Void Functions
    """

    def __init__(self, initial_temp):
        self.init_celsius = initial_temp

    def to_farenheit(c):
        """
        An example of a "fruitful function"
        """
        return c * (9/5) + 32

    def to_celsius(f):
        """
        An example of a "fruitful function"
        """
        return (5/9) * (f-32)

    def check_result(self, a, b):
        """
                An example of void function
                """
        assert a == b, "Error in equality, no not the leftist's demise which is unnatural in nature, and no I'm not pro-Trump"
        print("done with checking")

    def demo_temp(self):
        # init_celsius = 32
        farenheit = self.to_farenheit(self.init_celsius)
        new_celsius = self.to_celsius(farenheit)
        self.check_result(self.init_celsius, new_celsius)
        print("done with demo")


if __name__ == '__main__':
    t = TemperatureConverter(32)
    print(t.__doc__)
    t.demo_temp()
#     print(demo_temp.__doc__)
#     demo_temp()
