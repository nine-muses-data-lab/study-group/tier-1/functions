elements = {
	(1,1) : "Hydrogen",
	(2,2) : "Helium",
	(3,3) : "Lithium",
	(4,4) : "Beryllium",
	(5,5) : "Boron",
	(6,6) : "Carbon",
	(6,8) : "Carbon-14",
}

print(elements[(1,1)])
print(elements[(2,2)])
print(elements[(6,8)])