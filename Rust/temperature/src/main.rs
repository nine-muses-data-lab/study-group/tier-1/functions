fn to_farenheit(c: f32) -> f32 {
    c * (9f32 / 5f32) as f32 + 32f32
}

fn to_celsius(f: f32) -> f32 {
    (5f32 / 9f32) * (f - 32f32)
}

fn is_inverse<F, G, X, Y>(f: F, g: G, x: X) -> bool
where
    X: PartialEq + Copy,
    F: Fn(X) -> Y,
    G: Fn(Y) -> X,
{
    let y = f(x);
    let new_x = g(y);

    if x == new_x {
        true
    } else {
        false
    }
}

fn main() {
    let celsius: f32 = 32f32;
    let farenheit: f32 = to_farenheit(celsius);
    let new_celsius: f32 = to_celsius(farenheit);

    assert_eq!(celsius, new_celsius);

    assert!(is_inverse(to_farenheit, to_celsius, 35f32));
}
